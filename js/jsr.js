/**
 * Created by master on 01.03.16.
 */

function loadItems() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var json = JSON.parse(xhttp.responseText);
      
      for (var entry in json) {
          if (json.hasOwnProperty(entry)) {
              console.log(json[entry]);
              createItem(json[entry]);
          }
      }
        setListener();
    }
        
  };
  xhttp.open("GET", "data/listitems.json", true);
  xhttp.send();
}
function createItem(myobj) {
    // Elemente erstellen

    var divimg = document.createElement("div");
    var divoptions = document.createElement("div");
    var divclear = document.createElement("div");

    var li = document.createElement("li");
    var divsource = document.createElement("div");
    var divtitle = document.createElement("div");
    var divdate = document.createElement("div");
    var divtags = document.createElement("div");
    var divsubinfo = document.createElement("div");
    var divextra = document.createElement("div");
    // CSS Eigenschaften Klassen

    divimg.classList.add("image");
    divoptions.classList.add("button");
    divoptions.classList.add("options");
    divclear.classList.add("clear");

    divsource.classList.add("source");
    divdate.classList.add("date");
    divtitle.classList.add("title");
    divtags.classList.add("tags");
    divsubinfo.classList.add("subinfo");
    divextra.classList.add("extra");
    // Bildelement Eigenschaften
    divimg.style.backgroundImage = "url('" + myobj.src + "')";
    // Content den DIVs zuordnen
    divsource.appendChild(document.createTextNode(myobj.owner));
    divtitle.appendChild(document.createTextNode(myobj.name));
    divdate.appendChild(document.createTextNode(myobj.added));
    divtags.appendChild(document.createTextNode(myobj.numOfTags));

    // HTML Struktur aufbauen

    divsubinfo.appendChild(divsource);
    divsubinfo.appendChild(divdate);

    divextra.appendChild(divtags);
    divextra.appendChild(divoptions);

    li.appendChild(divimg);
    li.appendChild(divsubinfo);
    li.appendChild(divtitle);
    li.appendChild(divextra);
    li.appendChild(divclear);

    myview.appendChild(li);
}

function refreshItems() {
    while (myview.firstChild) {
        myview.removeChild(myview.firstChild);
    }
    loadItems();
}

function  addnewItem(name, owner, added, numOfTags, src) {
    createItem({name:name, owner:owner, added:added, numOfTags:numOfTags, src:src});
    setListener();
}

function loadNewItems() {

    // we initiate an xmlhttprequest and read out its body
    xhr("GET","data/listitems.json",null,function(xhr) {
        var textContent = xhr.responseText;
        console.log("loaded textContent from server: " + textContent);
        var jsonContent = JSON.parse(textContent);

        // we assume jsonContent is an array and iterate over its members
        jsonContent.forEach(function(contentItem){
            createListElementForContentItem(contentItem);
        });

    });

}

function createListElementForContentItem(item) {

    var li = document.createElement("li");
    li.textContent = item.name;
    var div = document.createElement("div");
    li.appendChild(div);
    div.classList.add("edit-item");
    div.classList.add("button");

    // add the element to the list
    document.getElementsByTagName("ul")[0].appendChild(li);

}