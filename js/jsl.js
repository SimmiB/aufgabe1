/**
 * Created by master on 01.03.16.
 */
var listenersSet;
//var view = "list";
var myview, layoutButton, title, refreshButton, newItemButton;
window.onload=function() {
    myview = document.getElementsByClassName("list").item(0);
    title = document.getElementById("listtitle");
    layoutButton = document.getElementById("layout");
    refreshButton = document.getElementsByClassName("refresh").item(0);
    newItemButton = document.getElementsByClassName("new-item").item(0);
    
    refreshButton.onclick = function (event) {
        refreshItems();
    }

    newItemButton.onclick = function (event) {
        addnewItem("My new Item", "stolen from lorempixel", (new Date()).toLocaleDateString(), 0, "http://lorempixel.com/300/300/");
    }
    
    loadItems();

    layoutButton.onclick = function (event) {
        if(layoutButton.classList.contains("listicon")) {
            layoutButton.classList.remove("listicon");
            layoutButton.classList.add("kachel");
            myview.addEventListener("transitionend", changeToList);
        }
        else {
            layoutButton.classList.remove("kachel");
            layoutButton.classList.toggle("listicon");
            myview.addEventListener("transitionend", changeToGrid);
        }
        myview.style.transition = "opacity 2s";
        myview.style.opacity = 0;
    }
}

// a function that reacts to the selection of a list item
function onListItemSelected(event) {
    // check in which phase we are
    if (event.eventPhase == Event.BUBBLING_PHASE) {
        // a helper function that looks up the target li element of the event
        function lookupEventTarget(el) {
            if (el.tagName.toLowerCase() == "li") {
                return el;
            }
            else if (el.tagName.toLowerCase() == "ul") {
                console.warn("lookupEventTarget(): we have already reached the list root!");
                return null;
            }
            else if (el.parentNode) {
                return lookupEventTarget(el.parentNode);
            }
        }

        // lookup the target of the event
        var eventTarget = lookupEventTarget(event.target);
        if (eventTarget) {
            // from the eventTarget, we find out the title of the list item, which is simply the text content of the li element
            showToast("selected: " + eventTarget.textContent);
        }
        else {
            showToast("list item target of event could not be determined!");
        }
    }
}

function toggleListeners() {


    // we set an onclick listener on the list view and check from which item the event was generated
    // we also set a listener on the '+'-button that loads content from the server!
    var ul = document.getElementsByTagName("ul")[0];
    var newItem = document.querySelector(".new-item");

    document.getElementsByTagName("body")[0].classList.toggle("listeners-active");

    if (listenersSet) {
        newItem.removeEventListener("click",loadNewItems);
        newItem.setAttribute("disabled","disabled");
        console.log("newItem.disabled: " + newItem.disabled);
        ul.removeEventListener("click", onListItemSelected);
        showToast("event listeners have been removed");
        listenersSet = false;
    }
    else {
        newItem.addEventListener("click",loadNewItems);
        newItem.removeAttribute("disabled");
        console.log("newItem.disabled: " + newItem.disabled);
        ul.addEventListener("click", onListItemSelected);
        showToast("event listeners have been set");
        listenersSet = true;
    }
}

/* show a toast and use a listener for transitionend for fading out */
function showToast(msg) {
    var toast = document.querySelector(".toast");
    if (toast.classList.contains("active")) {
        console.info("will not show toast msg " + msg + ". Toast is currently active, and no toast buffering has been implemented so far...");
    }
    else {
        console.log("showToast(): " + msg);
        toast.textContent = msg;
        /* cleanup */
        toast.removeEventListener("transitionend",finaliseToast);
        /* initiate fading out the toast when the transition has finished nach Abschluss der Transition */
        toast.addEventListener("transitionend", fadeoutToast);
        toast.classList.add("shown");
        toast.classList.add("active");
    }
}

function finaliseToast(event) {
    var toast = event.target;
    console.log("finaliseToast(): " + toast.textContent);
    toast.classList.remove("active");
}

/* trigger fading out the toast and remove the event listener  */
function fadeoutToast(event) {
    var toast = event.target;
    console.log("fadeoutToast(): " + toast.textContent);
    /* remove tranistionend listener */
    toast.addEventListener("transitionend", finaliseToast);
    toast.removeEventListener("transitionend", fadeoutToast);
    toast.classList.remove("shown");
}


function changeToGrid() {
    myview.classList.remove("list");
    myview.classList.toggle("grid");
    title.replaceChild(document.createTextNode("Kachelansicht"), title.firstChild);
    myview.removeEventListener("transitionend", changeToGrid);
    myview.style.transition = "opacity 1s";
    myview.style.opacity = 1;
}

function changeToList() {
    myview.classList.remove("grid");
    myview.classList.toggle("list");
    title.replaceChild(document.createTextNode("Listenansicht"), title.firstChild);
    myview.removeEventListener("transitionend", changeToList);
    myview.style.transition = "opacity 1s";
    myview.style.opacity = 1;
}

function clickEntry() {
    for (i = 0; i < this.childNodes.length; i++) {
        if(typeof this.childNodes.item(i).classList === "object") {
            if(this.childNodes.item(i).classList.contains("title")) {
                alert(this.childNodes.item(i).textContent);
            }
        }
    }
}

function clickOptions(event) {
    event.stopPropagation();
    var title, url;
    var parent =  this.parentElement.parentElement;
    for (i = 0; i < parent.childNodes.length; i++) {
        if(typeof parent.childNodes.item(i).classList === "object") {
            if(parent.childNodes.item(i).classList.contains("title")) {
                title = parent.childNodes.item(i).textContent;
            }
            if(parent.childNodes.item(i).classList.contains("image")) {
               url = window.getComputedStyle(parent.childNodes.item(i)).getPropertyValue("background-image").replace('url("','').replace('")','');
            }
        }
    }
    alert("Title: " + title + " URL: " + url);
    myview.removeChild(parent);
}
function setListener() {
    for (i = 0; i < myview.childNodes.length; i++) {
        myview.childNodes.item(i).addEventListener("click", clickEntry);
        for (n = 0; n < myview.childNodes.item(i).childNodes.length; n++) {
            if (typeof myview.childNodes.item(i).childNodes.item(n).classList === "object") {
                if (myview.childNodes.item(i).childNodes.item(n).classList.contains("extra")) {
                    for (m = 0; m < myview.childNodes.item(i).childNodes.item(n).childNodes.length; m++) {
                        if (typeof myview.childNodes.item(i).childNodes.item(n).childNodes.item(m).classList === "object") {
                            if (myview.childNodes.item(i).childNodes.item(n).childNodes.item(m).classList.contains("options")) {
                                myview.childNodes.item(i).childNodes.item(n).childNodes.item(m).addEventListener("click", clickOptions);
                            }
                        }
                    }
                }
            }
        }
    }
}